//
//  Stats.cpp
//  RayTracing
//
//  Created by Dani Barca on 10/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#include "Stats.h"

Stats *Stats::instance;
std::string Stats::resolution;
int Stats::nObjects;
int Stats::nLights;

int Stats::raysTraced;
int Stats::raysTracedCam;
int Stats::shadeRaysTraced;
int Stats::reflectRaysTraced;
int Stats::octreeNodes;
int Stats::intersectionsTested;
int Stats::positiveIntersections;
  
Stats::Stats(){
  assert(instance == NULL);
    
  instance = this;
    
  raysTraced = 0;
}

Stats* Stats::getInstance(){
  if(instance == NULL){
    Stats();
  }
  return instance;
}

void Stats::setResolution(std::string res){
  resolution = res;
}

void Stats::setNObjects(int n){
  nObjects = n;
}

void Stats::setNLights(int n){
  nLights = n;
}

void Stats::rayTraced(){
  raysTraced++;
}

void Stats::rayTracedCamera(){
  raysTracedCam++;
}

void Stats::shadeRayTraced(){
  shadeRaysTraced++;
}

void Stats::reflectRayTraced() {
  reflectRaysTraced++;
}

void Stats::octreeNodeCreated(){
  octreeNodes++;
}

void Stats::intersectionTested() {
  intersectionsTested++;
}

void Stats::intersectionTestedPositive() {
  positiveIntersections++;
}

void Stats::clear(){
  resolution = "";
  nObjects = 0;
  nLights = 0;
  
  raysTracedCam = 0;
  raysTraced = 0;
  octreeNodes = 0;
  shadeRaysTraced = 0;
  reflectRaysTraced = 0;
  intersectionsTested = 0;
  positiveIntersections = 0;
}

void Stats::print(){
  std::cout << " -- " << std::endl;
  
  std::cout << "Resolution: " << resolution << std::endl;
  std::cout << "Objects num: "<< nObjects << std::endl;
  std::cout << "Lights num: " << nLights << std::endl;
  
  std::cout << "Rays Traced: " << raysTraced << std::endl;
  std::cout << "Rays Traced from camera: " << raysTracedCam << std::endl;
  std::cout << "Rays Traced for shade: " << shadeRaysTraced << std::endl;
  std::cout << "Rays Traced for reflection: " << reflectRaysTraced << std::endl;
  std::cout << "Intersections tested: " << intersectionsTested << std::endl;
  std::cout << "Positive intersections: " << positiveIntersections  << " -- " << positiveIntersections * 100 / intersectionsTested << "%" << std::endl;
  //std::cout << "Octree Nodes Created: " << octreeNodes << std::endl;
}
