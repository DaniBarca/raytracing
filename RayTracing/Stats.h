//
//  Stats.h
//  RayTracing
//
//  Created by Dani Barca on 11/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef RayTracing_Stats_h
#define RayTracing_Stats_h

#include <cstdio>
#include <assert.h>
#include <iostream>

class Stats{
  static Stats *instance;
  
  static std::string resolution;
  static int nObjects;
  static int nLights;
  
  static int raysTracedCam;
  static int raysTraced;
  static int octreeNodes;
  static int shadeRaysTraced;
  static int reflectRaysTraced;
  static int intersectionsTested;
  static int positiveIntersections;
  
  Stats();
  
public:
  static Stats* getInstance();
  
  static void setResolution(std::string res);
  static void setNObjects(int n);
  static void setNLights(int n);
  
  static void rayTracedCamera();
  static void rayTraced();
  static void shadeRayTraced();
  static void reflectRayTraced();
  static void intersectionTested();
  static void intersectionTestedPositive();
  
  static void octreeNodeCreated();
  
  static void clear();
  
  static void print();
};

#endif
