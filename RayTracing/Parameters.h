//
//  Parameters.h
//  RayTracing
//
//  Created by Dani Barca on 15/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef RayTracing_Parameters_h
#define RayTracing_Parameters_h

#define LOAD_OCTREE false

#define AMBIENT_REFR_INDEX 1.000277

#endif
