#include "raytracer.h"

/*-<==>-----------------------------------------------------------------
/ 
/----------------------------------------------------------------------*/
CSolidMaterial::CSolidMaterial(const COLOR &diffuse, SCALAR reflectance, SCALAR specular, SCALAR gloss, SCALAR refractive_index) {
  diffuse_color = diffuse;
  reflectance_factor = reflectance;
  specular_constant = specular;
  this->gloss = gloss;
  this->refractive_index = refractive_index;
}

/*-<==>-----------------------------------------------------------------
/ Diffuse and reflectance parameters are independent of the position
/----------------------------------------------------------------------*/
COLOR  CSolidMaterial::getDiffuseColor(const VECTOR &loc) const { 
  return this->diffuse_color;
}

SCALAR CSolidMaterial::getReflectance(const VECTOR &loc)  const {
  return this->reflectance_factor;
}

SCALAR CSolidMaterial::getSpecular() const{
  return this->specular_constant;
}

SCALAR CSolidMaterial::getGloss() const{
  return this->gloss;
}

SCALAR CSolidMaterial::getRefractiveIndex() const{
  return this->refractive_index;
}
