//
//  O_Node.h
//  RayTracing
//
//  Created by Dani Barca on 11/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef __RayTracing__O_Node__
#define __RayTracing__O_Node__

#include <stdio.h>
#include <Vector>
#include "geometry.h"
#include "rtobject.h"
#include <math.h>
#include <iostream>
#include "Stats.h"
#include "Octree.h"

class O_Node {
  friend class Octree;
  
  static int id_inc;
  int id;
  int depth;
  
  bool is_leaf;
  bool is_root;
  
  
  std::vector<O_Node*> children;
  
  O_Node* parent = NULL;

  std::vector<CRTObject*> objects;
  
  SCALAR min_x;
  SCALAR min_y;
  SCALAR min_z;
  SCALAR max_x;
  SCALAR max_y;
  SCALAR max_z;
  
  void newNode();
  
  bool is_colliding(CRTObject* o);
  bool is_containing(CRTObject*  o);
  bool is_containing(VECTOR point);
  
public:
  O_Node(SCALAR min_x, SCALAR min_y, SCALAR min_z,SCALAR max_x, SCALAR max_y, SCALAR max_z, O_Node* parent = NULL, bool root = false);
  
  bool checkAndPush(CRTObject* o);
  std::vector<CRTObject*> getObjects(CLine* line,SCALAR* return_t);
  
  bool node_ray_collision(CLine* line, SCALAR* t_max);
 // bool object_ray_collision(CRTObject* o, CLine* line);
  
  void push(CRTObject* o);
};

#endif /* defined(__RayTracing__O_Node__) */
