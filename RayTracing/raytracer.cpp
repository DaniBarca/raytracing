#include <assert.h>
#include "raytracer.h"
#include "image.h"
#include "Parameters.h"

/*-<==>-----------------------------------------------------------------
/ 
/----------------------------------------------------------------------*/
CRayTracer::CRayTracer() 
: max_recursion_level( 10 )
, background_color( COLOR(0,0,0) )
{}

/*-<==>-----------------------------------------------------------------
/ Create an image, and for each pixel in the screen create a line
/ and retrieve which color is seen through this line
/ Save the image to a file
/----------------------------------------------------------------------*/
void CRayTracer::render(std::string path) {
  CBitmap* img = new CBitmap(camera.getXRes(), camera.getYRes(), 24);
  
  CLine ray;
  
  int xRes = camera.getXRes();
  int yRes = camera.getYRes();
  
  Stats::setResolution(std::to_string(xRes) + "x" + std::to_string(yRes));
  Stats::setNObjects(objects.size());
  Stats::setNLights(lights.size());
  
  for(int y = 0; y < yRes; ++y){
    for(int x = 0; x < xRes; ++x){
      ray = camera.getLineAt(x, y);
      Stats::rayTracedCamera();
      this->trace(ray);
      
      if(ray.color.x > 1)
        ray.color.x = 1;
      if(ray.color.y > 1)
        ray.color.y = 1;
      if(ray.color.z > 1)
        ray.color.z = 1;
      
      img->setPixel(x, y, ray.color);
    }
  }
  
  
  img->saveTGA(path.c_str());
}

/*-<==>-----------------------------------------------------------------
/ Find which object and at which 't' the line hits and object
/ from the scene.
/ Returns true if the object hits some object
/----------------------------------------------------------------------*/
bool CRayTracer::intersects(CLine &line) {
  SCALAR intersection_t;
  
  VRTObjects obs = LOAD_OCTREE ? Octree::getInstance()->getObjects(&line) : VRTObjects();
  
  VRTObjects::iterator i = obs.begin();
  LRTObjects::iterator il= objects.begin();
  //std::cout << "num its: " << obs.size() << std::endl;
  
  CRTObject *obj;
  bool first = true;
  
  while( (LOAD_OCTREE && i != obs.end()) || (!LOAD_OCTREE && il != objects.end())) {
    obj = LOAD_OCTREE ? *i++ : *il++;
    Stats::intersectionTested();
    if(obj->hits(line, intersection_t)){
      Stats::intersectionTestedPositive();
      if(LOAD_OCTREE || (!LOAD_OCTREE && (line.t > intersection_t || first))){
        first = false;
        line.t = intersection_t;
        line.obj = obj;
        
        if(LOAD_OCTREE) return true;
      }
    }
  }
  
  return !first;
}

bool CRayTracer::getsLight(VECTOR point,CLight light){
  Stats::shadeRayTraced();
  CLine line = CLine();
  line.dir = (light.getLocation() - point).unit();
  line.loc = point;
  
  SCALAR intersection_t = (light.getLocation() - point).length();
  
  VRTObjects obs = LOAD_OCTREE ? Octree::getInstance()->getObjects(&line) : VRTObjects();
  
  VRTObjects::iterator i = obs.begin();
  LRTObjects::iterator il= objects.begin();
  //std::cout << "num its: " << obs.size() << std::endl;
  
  CRTObject *obj;
  
  while( (LOAD_OCTREE && i != obs.end()) || (!LOAD_OCTREE && il != objects.end())) {
    obj = LOAD_OCTREE ? *i++ : *il++;
    Stats::intersectionTested();
    if(obj->hits(line, intersection_t)){
      Stats::intersectionTestedPositive();
      if(line.t < intersection_t){
        return false;
      }
    }
  }
  
  return true;
}

/*-<==>-----------------------------------------------------------------
/ Returns in line.color the color captured by the line.
/----------------------------------------------------------------------*/
void CRayTracer::trace(CLine &line, int depth) {
  if(depth > max_recursion_level) return;
  Stats::rayTraced();
  
  if(depth > 0)
    Stats::reflectRayTraced();
  
  CLine actualLine = line;
  CLine rLine;
  VECTOR auxColor;
  
  VECTOR norm;
  VECTOR lDir;
  VECTOR rDir;
  VECTOR vDir;
  VECTOR intersection;
  
  SCALAR diffuse_ammount;
  SCALAR specular_ammount;
  CLight* light;
  
  SCALAR reflectance;
  
  COLOR I;
  std::list<CLight*>::iterator li;
  if(intersects(actualLine)){
    
    norm = actualLine.obj->getNormal(actualLine.getIntersection());
    intersection = actualLine.getIntersection();
    
    li = lights.begin();
    while(li != lights.end()){
      light = *li++;
      
      if(!getsLight(intersection, *light)){
        //line.addColor(light->getColor().filter( actualLine.obj->getMaterial()->getDiffuseColor( line.getIntersection() ) + this->ambient));
        continue;
      }
      
      lDir = (light->getLocation() - intersection);
      lDir.normalize();
      rDir = 2 * (norm.dot(lDir)) * norm - lDir;
      
      vDir = line.dir;
      
      diffuse_ammount = norm.dot(lDir);
      specular_ammount= rDir.dot(lDir);
      
      if(specular_ammount <= 0)
        specular_ammount = 0;
      if(diffuse_ammount <= 0)
        diffuse_ammount = 0;
      
      I+= light->getColor().filter( actualLine.obj->getMaterial()->getDiffuseColor( line.getIntersection() ))
        * (diffuse_ammount + actualLine.obj->getMaterial()->getSpecular() * std::pow(specular_ammount,actualLine.obj->getMaterial()->getGloss()));
      
      //I+= this->ambient;
      
      line.addColor(I);
      
      rLine.dir = rDir;
      rLine.loc = intersection;
      
      reflectance = actualLine.obj->getMaterial()->getReflectance(VECTOR(0,0,0));
      
      trace(rLine,depth+1);
      
      if(actualLine.obj->getMaterial()->getRefractiveIndex() > 0){
        
      }
      
      
      line.addColor(rLine.color*(reflectance));
    }
  } else {
    line.addColor(background_color);
    return;
  }
    
    //line.addColor(line.obj->getIntensity(line, lights, rDir));
}

/*-<==>-----------------------------------------------------------------
/ Default background 
/----------------------------------------------------------------------*/
void CRayTracer::background(CLine &line) {
  line.color = background_color;
}

