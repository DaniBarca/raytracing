#include "plane.h"

/*-<==>-----------------------------------------------------------------
/ n.x * x + n.y * y + n.z * z = d
/----------------------------------------------------------------------*/
CPlane::CPlane (const VECTOR &normal, SCALAR distance) : norm(normal), dist(distance) {
  this->is_finite = false;
  
  this->min_x = -1;
  this->min_y = -1;
  this->min_z = -1;
  
  this->max_x = -1;
  this->max_y = -1;
  this->max_z = -1;
}

bool CPlane::hits (const CLine &line, SCALAR &t_hit) {
  if (line.dir.dot(norm) !=  0){
    t_hit = (((VECTOR(fabs(norm.x)*dist, fabs(norm.y)*dist, fabs(norm.z)*dist) - line.loc)).dot(norm)) / norm.dot(line.dir);

    if (t_hit > SMALL_AMOUNT){
      return true;
    }
  }
  return false;
	return false;
}

VECTOR CPlane::getNormal (const VECTOR &loc) {
  return norm;
}

