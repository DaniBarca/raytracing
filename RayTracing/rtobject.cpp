#include "raytracer.h"

CRTObject::CRTObject() : loc(0,0,0), material(NULL), min_x(0),min_y(0),min_z(0),max_x(0),max_y(0),max_z(0),is_finite(false)
{
	// Init members
}

CRTObject::~CRTObject() {
	// Nothing special to be done
}

/*-<==>-----------------------------------------------------------------
/ Save new position of the object
/----------------------------------------------------------------------*/
void CRTObject::setLocation(const VECTOR &new_loc) {
  loc = new_loc;
}

/*-<==>-----------------------------------------------------------------
/ Save the pointer to the current material of this object
/----------------------------------------------------------------------*/
void CRTObject::setMaterial (CMaterial *new_material) { 
	material = new_material; 
}


COLOR CRTObject::getIntensity(CLine line, LLights lights,CLine& rDirResp){
  VECTOR norm = this->getNormal(line.getIntersection());
  VECTOR lDir;
  VECTOR rDir;
  VECTOR vDir;
  
  VECTOR intersection = line.getIntersection();
  COLOR I = COLOR();
  
  rDirResp.loc = intersection;
  
  std::list<CLight*>::iterator i = lights.begin();
  
  SCALAR diffuse_amount;
  SCALAR specular_amount;
  
  CLight *light;
  while(i != lights.end() ){
    light = *i++;
    
    lDir = (light->getLocation() - intersection);
    lDir.normalize();
    rDir = 2 * (norm.dot(lDir)) * norm - lDir;
    vDir = line.dir;
    
    rDirResp.dir = rDir;
    
    diffuse_amount = norm.dot( lDir );
    specular_amount = rDir.dot(lDir);
    
    if(specular_amount <= 0)
      specular_amount = 0;
    if(diffuse_amount <= 0)
      diffuse_amount = 0;
    
    I += light->getColor().filter( getMaterial()->getDiffuseColor( line.getIntersection() ))
      * (diffuse_amount + getMaterial()->getSpecular() * std::pow(specular_amount,getMaterial()->getGloss()));
  }
  
  return I;
}
