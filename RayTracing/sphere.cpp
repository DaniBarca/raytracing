#include "sphere.h"

/*-<==>-----------------------------------------------------------------
/ Constructor
/----------------------------------------------------------------------*/
CSphere::CSphere(SCALAR a_radius) 
: radius( a_radius ) {
  this->is_finite = true;
  this->min_x = loc.x - radius;
  this->min_y = loc.y - radius;
  this->min_z = loc.z - radius;
  
  this->max_x = loc.x + radius;
  this->max_y = loc.y + radius;
  this->max_z = loc.z + radius;
}

/*-<==>-----------------------------------------------------------------/
/
/----------------------------------------------------------------------*/
bool CSphere::hits (const CLine &line, SCALAR &t_hit) {
  double t_in, t_out = 0;
  double b = (line.loc - this->loc).dot(line.dir);
  double c = (line.loc - this->loc).dot(line.loc - this->loc) - radius*radius;
  double A = b*b - c;
  if (A >= 0){
    A = sqrt(A);
    t_in = -b - A;
    t_out = -b + A;
    if (t_in > SMALL_AMOUNT){
      t_hit = t_in;
      return true;
    }
    else if ( t_out > SMALL_AMOUNT){
      t_hit = t_out;
      return true;
    }
  }
  return false;
}

VECTOR CSphere::getNormal(const VECTOR &hit_loc) {
  return (hit_loc - loc).unit();
  return VECTOR(0,0,0);
}

void CSphere::setLocation(const VECTOR &loc){
  CRTObject::setLocation(loc);

  this->min_x = loc.x - radius;
  this->min_y = loc.y - radius;
  this->min_z = loc.z - radius;
  
  this->max_x = loc.x + radius;
  this->max_y = loc.y + radius;
  this->max_z = loc.z + radius;
}
