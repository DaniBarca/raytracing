//
//  cylinder.h
//  RayTracing
//
//  Created by Dani Barca on 21/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef __RayTracing__cylinder__
#define __RayTracing__cylinder__

#include <stdio.h>

#include "raytracer.h"
#include<math.h>

class CCylinder : public CRTObject {
  SCALAR radius;
  SCALAR length;
  
  VECTOR A, B;
  
  VECTOR lastNormal = VECTOR(0,0,0);
public:
  CCylinder(SCALAR a_radius, SCALAR a_length);
  bool hits (const CLine &line, SCALAR &hits);
  VECTOR getNormal (const VECTOR &loc);
  
  void setLocation(const VECTOR &loc);
};


#endif /* defined(__RayTracing__cylinder__) */
