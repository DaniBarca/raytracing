//
//  cylinder.cpp
//  RayTracing
//
//  Created by Dani Barca on 21/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#include "cylinder.h"

/*-<==>-----------------------------------------------------------------
 / Constructor
 /----------------------------------------------------------------------*/
CCylinder::CCylinder(SCALAR a_radius, SCALAR a_length)
: radius( a_radius ),length(a_length) {
  this->is_finite = true;
  
/*  this->min_x = loc.x - radius;
  this->min_y = loc.y - radius;
  this->min_z = loc.z - radius;
  
  this->max_x = loc.x + radius;
  this->max_y = loc.y + radius;
  this->max_z = loc.z + radius;*/
}

/*-<==>-----------------------------------------------------------------/
 /
 /----------------------------------------------------------------------*/
bool CCylinder::hits (const CLine &line, SCALAR &t_hit) {
  SCALAR cxmin, cymin, czmin, cxmax, cymax, czmax;
  
  
  
  if (A.z < B.z) { czmin = A.z - radius; czmax = B.z + radius; } else { czmin = B.z - radius; czmax = A.z + radius; }
  if (A.y < B.y) { cymin = A.y - radius; cymax = B.y + radius; } else { cymin = B.y - radius; cymax = A.y + radius; }
  if (A.x < B.x) { cxmin = A.x - radius; cxmax = B.x + radius; } else { cxmin = B.x - radius; cxmax = A.x + radius; }
  
  VECTOR AB = B - A;
  VECTOR AO = line.loc - A;
  VECTOR AOxAB = AO.cross(AB);
  VECTOR VxAB = line.dir.cross(AB);
  SCALAR ab2 = AB.dot(AB);
  SCALAR a = VxAB.dot(VxAB);
  SCALAR b = 2 * VxAB.dot(AOxAB);
  SCALAR c = AOxAB.dot(AOxAB) - (radius * radius * ab2);
  SCALAR d = b * b - 4 * a * c;
  
  if(d < 0) return false;
  SCALAR time = (-b - sqrt(d)) / (2 * a);
  if(time < 0) return false;
  
  VECTOR intersection = line.loc + line.dir * time;
  VECTOR projection   = A + (AB.dot(intersection - A) / ab2) * AB;
  if((projection - A).length() + (B - projection).length() > AB.length()) return false;
  
  lastNormal = (intersection - projection);
  lastNormal.normalize();
  t_hit = time;
  
  return true;
}

VECTOR CCylinder::getNormal(const VECTOR &hit_loc) {
  if(lastNormal != VECTOR(0,0,0)){
    return lastNormal;
  }
  return (hit_loc - loc).unit();
  return VECTOR(0,0,0);
}

void CCylinder::setLocation(const VECTOR &loc){
  CRTObject::setLocation(loc);
  
  this->A = this->loc;
  this->B = this->loc + VECTOR(0,1,0) * length;
  
/*  this->min_x = loc.x - radius;
  this->min_y = loc.y - radius;
  this->min_z = loc.z - radius;
  
  this->max_x = loc.x + radius;
  this->max_y = loc.y + radius;
  this->max_z = loc.z + radius;*/
}