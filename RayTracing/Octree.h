//
//  Octree.h
//  RayTracing
//
//  Created by Dani Barca on 11/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef __RayTracing__Octree__
#define __RayTracing__Octree__

#include <stdio.h>
#include <assert.h>
#include <Vector>
#include "O_Node.h"
#include <iostream>

class O_Node;

class Octree {
  friend class O_Node;
  
  static Octree* instance;
  static O_Node* rootNode;
  
  bool loaded;
  bool first;
  
  SCALAR min_x;
  SCALAR min_y;
  SCALAR min_z;
  SCALAR max_x;
  SCALAR max_y;
  SCALAR max_z;
  
  std::vector<CRTObject*> no_octree_objs;
  std::vector<CRTObject*> object_list;
  
  Octree();
  
  static int max_depth;
  
public:
  static Octree* getInstance();
  
  static void setMaxDepth(int max);
  
  void push(CRTObject* o);
  void load();
  
  std::vector<CRTObject*> getObjects(CLine* line);
};

#endif /* defined(__RayTracing__Octree__) */
