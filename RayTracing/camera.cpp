#include "raytracer.h"

/*-<==>-----------------------------------------------------------------
/ 
/---------------------------------------------------------------------*/
CCamera::CCamera() {
  // Initialize with some default parameters
  setRenderParameters(320, 240, 60.0f);
  lookAt(VECTOR(0,0,0), VECTOR(0,0,1));
}

CCamera::~CCamera() {
}

/*-<==>-----------------------------------------------------------------
/ Save render parameters for later use
/ fov is in degrees
/---------------------------------------------------------------------*/
void CCamera::setRenderParameters (int axres, int ayres, SCALAR afov_in_deg) {
  // Pendiente de implementar correctamente
  // ...
  // Compute view_d from afov_in_deg
  // ...
  this->xres = axres;
  this->yres = ayres;
  
  this->fov = DEGTORAD(afov_in_deg);
  
  this->viewd = yres * 0.5 / atan(this->fov * 0.5);
}

/*-<==>-----------------------------------------------------------------
/ Save the new camera position and target point. 
/ Define the axis of the camera (front, up, left) in world coordinates 
/ based on the current values of the vectors target & loc 
/---------------------------------------------------------------------*/
void CCamera::lookAt(const VECTOR &src_point, const VECTOR &dst_point) {
  loc = src_point;
  target = dst_point;
  
  front = (target-loc);
  front.normalize();
  
  up    = VECTOR(1.0,0.0,0.0).cross(front);
  left  = (front).cross(up);
}

VECTOR CCamera::getLocation(){
  return loc;
}

/*-<==>-------------------------------------------------------------------
/ return a line which starts on camera position and goes through the pixel
/ (x,y) from the screen
/-----------------------------------------------------------------------*/
CLine CCamera::getLineAt (SCALAR x, SCALAR y) const {
  //Get the Screen width
  //double w = (2*viewd*cos(DEGTORAD(fov)))/sin(DEGTORAD(fov));
  
  //Calcular Centro: distancia * frontal
  VECTOR C = viewd * front;
  
  //Calcular coordenada y: usando el vector up
  VECTOR yC = up * (yres*0.5 - y - 0.5);
  
  //Calcular coordenada x: usando el vector left
  VECTOR xC = left * (xres*0.5 - x -0.5);
  
  //El punto del píxel es:
  VECTOR pCoor = C + yC + xC;
  
  return CLine(loc,pCoor);
}
