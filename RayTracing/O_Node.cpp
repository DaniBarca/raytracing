//
//  O_Node.cpp
//  RayTracing
//
//  Created by Dani Barca on 11/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#include "O_Node.h"

int O_Node::id_inc = 0;

O_Node::O_Node(SCALAR min_x, SCALAR min_y, SCALAR min_z,SCALAR max_x, SCALAR max_y, SCALAR max_z, O_Node* parent, bool root)
            : min_x(min_x),min_y(min_y),min_z(min_z),max_x(max_x),max_y(max_y),max_z(max_z)
{
  children = std::vector<O_Node*>();
  is_leaf = true;
  id = id_inc++;
  
  this->parent = parent;
  this->is_root = root;
  
  if(parent == NULL){
    depth = 1;
  } else {
    depth = parent->depth+1;
  }
  
  Stats::octreeNodeCreated();
}

bool O_Node::is_colliding(CRTObject *o){
  if(!o->is_finite) return false;
  
  return ( max_x >= o->min_x && min_x <= o->max_x &&
           max_y >= o->min_y && min_y <= o->max_y &&
           max_z >= o->min_z && min_z <= o->max_z);
}

bool O_Node::is_containing(CRTObject *o){
  if(!o->is_finite) return false;
  
  return ( max_x > o->max_x && min_x < o->min_x &&
           max_y > o->max_y && min_y < o->min_y &&
           max_z > o->max_z && min_z < o->min_z);
}

bool O_Node::is_containing(VECTOR point){
  return (point.x >= min_x && point.x <= max_x &&
          point.y >= min_y && point.y <= max_y &&
          point.z >= min_z && point.z <= max_z);
}

bool O_Node::checkAndPush(CRTObject* o){
  if(is_colliding(o)){
    if(is_containing(o)){
      if(children.size() == 0){
        if(Octree::max_depth == -1 || (Octree::max_depth != -1 && Octree::max_depth > depth))
          newNode();
        else
          this->push(o);
      }
      for(int i = 0; i < children.size(); ++i){
        children[i]->checkAndPush(o);
      }
    } else {
      if(parent != NULL){
        parent->push(o);
      }
      if(is_root){
        this->push(o);
      }
      //this->push(o);
    }
    return true;
  }
  return false;
}

void O_Node::push(CRTObject* o){
  objects.push_back(o);
}

std::vector<CRTObject*> O_Node::getObjects(CLine* line,SCALAR* return_t){
  SCALAR tmax;
  SCALAR child_t;
  if(!node_ray_collision(line,&tmax)) return std::vector<CRTObject*>();
  
  *return_t = tmax;
  
  std::vector<CRTObject*> res = objects;
  
  std::vector<CRTObject*> aux;
  //stack tree iteration (filo) TODO
  if(!is_leaf){
    for(int i = 0; i < children.size(); ++i){
      if((aux = children[i]->getObjects(line,&child_t)).size() > 0){
        if(child_t > tmax){
          res.insert(res.end(),aux.begin(),aux.end());
        } else {
          aux.insert(aux.end(),res.begin(),res.end());
          res = aux;
        }
      }
    }
  }
  return res;
}

bool O_Node::node_ray_collision(CLine* line, SCALAR* t_max){
  //std::cout << "line dir: " << line->dir.x << " loc: " << line->loc.x << std::endl;
  
  if(is_containing(line->loc)){
    return true;
  }
  
  double t1 = (min_x - line->loc.x)*line->inv_dir.x;
  double t2 = (max_x - line->loc.x)*line->inv_dir.x;
  
  double tmin = t1 > t2 ? t2 : t1;
  double tmax = t1 > t2 ? t1 : t2;
  
  t1 = (min_y - line->loc.y)*line->inv_dir.y;
  t2 = (max_y - line->loc.y)*line->inv_dir.y;
  
  double aux = t1 > t2 ? t2 : t1;
  tmin = tmin > aux ? tmin : aux;
  
  aux = t1 > t2 ? t1 : t2;
  tmax = tmax > aux ? aux : tmax;
  
  t1 = (min_z - line->loc.z)*line->inv_dir.z;
  t2 = (max_z - line->loc.z)*line->inv_dir.z;
  
  aux = t1 > t2 ? t2 : t1;
  tmin = tmin > aux ? tmin : aux;
  
  aux = t1 > t2 ? t1 : t2;
  tmax = tmax > aux ? aux : tmax;
  
  //std::cout << "tmax: " << tmax << " tmin " << tmin << std::endl;
  
  *t_max = tmin;
  
  return tmax >= tmin;
}

void O_Node::newNode(){
  
  is_leaf = false;
  O_Node* N_AUX;
  
  SCALAR middle_x = (min_x + max_x)*0.5;
  SCALAR middle_y = (min_y + max_y)*0.5;
  SCALAR middle_z = (min_z + max_z)*0.5;

  N_AUX = new O_Node(min_x,
                     min_y,
                     min_z,
                     middle_x,
                     middle_y,
                     middle_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(middle_x,
                     min_y,
                     min_z,
                     max_x,
                     middle_y,
                     middle_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(min_x,
                     middle_y,
                     min_z,
                     middle_x,
                     max_y,
                     middle_z,
                     this);
  children.push_back(N_AUX);

  N_AUX = new O_Node(middle_x,
                     middle_y,
                     min_z,
                     max_x,
                     max_y,
                     middle_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(min_x,
                     min_y,
                     middle_z,
                     middle_x,
                     middle_y,
                     max_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(middle_x,
                     min_y,
                     middle_z,
                     max_x,
                     middle_y,
                     max_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(min_x,
                     middle_y,
                     middle_z,
                     middle_x,
                     max_y,
                     max_z,
                     this);
  children.push_back(N_AUX);
  
  N_AUX = new O_Node(middle_x,
                     middle_y,
                     middle_z,
                     max_x,
                     max_y,
                     max_z,
                     this);
  children.push_back(N_AUX);
  
}
