#include "raytracer.h"
#include "sphere.h"
#include "plane.h"
#include "Stats.h"
#include "Octree.h"
#include "Parameters.h"

/*bool CRayTracer::loadSnowflake (const char *filename) {
  this->max_recursion_level = 3;
  
  this->ambient = COLOR(0.1,0.1,0.1);
  
  FILE *f = fopen (filename, "r");
  if (!f)
    return false;
  
  // Add the camera looking at the origin
  camera.lookAt (VECTOR(0, 0, 2), VECTOR (0,0,0));
  camera.setRenderParameters (1024,1024,45);
  
  // Define background color
  background_color = COLOR( 0.078, 0.361, 0.753 );
  
  // Add a two material
  materials["txt001"]    = new CSolidMaterial (COLOR (0.8, 0.6, 0.264), 0, 0.8, 50);
  materials["txt002"]    = new CSolidMaterial (COLOR (0.5, 0.45, 0.35), 0.1, 0.8, 90);
  //materials["txt002"]    = new CSolidMaterial (COLOR (1, 0, 0), 0);
  
  // Add the ground
  CPlane *plane = new CPlane (VECTOR(0,1,0), -0.5);
  plane->setMaterial (materials["txt001"]);
  LOAD_OCTREE ? Octree::getInstance()->push(plane) : objects.push_back (plane);
  
  // This is a very simply parser!!
  while (!feof(f)) {
    char buf[512];
    fgets (buf, 511, f);
    if (strncmp (buf, "sphere", 6) == 0) {
      char material[64];
      double x,y,z, rad;
      sscanf (buf, "sphere %s %lf %lf %lf %lf\n", material, &rad, &x,&y,&z);
      CSphere *sph = new CSphere(rad);
      sph->setLocation (VECTOR(x,z,y));
      sph->setMaterial (materials["txt002"]);
      
      LOAD_OCTREE ? Octree::getInstance()->push(sph) : objects.push_back (sph);
    }
  }
  
  // Add 3 white lights
  lights.push_back (new CLight(VECTOR ( 4, 2, 3), COLOR (1,1,1)));
  //lights.push_back (new CLight(VECTOR ( 1, 4,-4), COLOR (1,1,1)));
  //lights.push_back (new CLight(VECTOR (-3, 5, 1), COLOR (1,1,1)));
  
  if(LOAD_OCTREE){
    Octree::getInstance()->load();
    std::cout << "Octree loaded" << std::endl;
  }
  
  fclose (f);
  return true;
}*/

bool CRayTracer::loadSnowflake (const char *filename) {
  FILE *f = fopen (filename, "r");
  if (!f)
    return false;
  objects.clear();
  // Add the camera looking at the origin
  camera.lookAt (VECTOR(2.1, 1.7, 1.3), VECTOR (0,0,0));
  camera.setRenderParameters (1024,1024,45);
  
  // Define background color
  background_color = COLOR( 0.078, 0.361, 0.753 );
  
  // Add a two material
  materials["txt001"]    = new CSolidMaterial (COLOR (0.8, 0.6, 0.264), 0, 0, 0);
  materials["txt002"]    = new CSolidMaterial (COLOR (0.5, 0.45, 0.35), 0.5, 0, 0);
  
  // Add the ground
  CPlane *plane = new CPlane (VECTOR(0,1,0), -0.5);
  plane->setMaterial (materials["txt001"]);
  objects.push_back (plane);
  
  // This is a very simply parser!!
  while (!feof(f)) {
    char buf[512];
    fgets (buf, 511, f);
    if (strncmp (buf, "sphere", 6) == 0) {
      char material[64];
      double x,y,z, rad;
      sscanf (buf, "sphere %s %lf %lf %lf %lf\n", material, &rad, &x,&y,&z);
      CSphere *sph = new CSphere(rad);
      sph->setLocation (VECTOR(x,z,y));
      sph->setMaterial (materials["txt002"]);
      objects.push_back (sph);
    }
  }
  
  // Add 3 white lights
  lights.push_back (new CLight(VECTOR ( 4, 2, 3), COLOR (1,1,1)));
  lights.push_back (new CLight(VECTOR ( 1, 4,-4), COLOR (1,1,1)));
  lights.push_back (new CLight(VECTOR (-3, 5, 1), COLOR (1,1,1)));
  
  fclose (f);
  return true;
}

/*-<==>-----------------------------------------------------------------
/ Defines the scene
/----------------------------------------------------------------------*/
void CRayTracer::load (int it) {
  
  this->max_recursion_level = 10;

  objects.clear();
  
  // Add the camera looking at the origin
  camera.lookAt (VECTOR(300, 120, 120), VECTOR (0,0,0));
  //camera.lookAt (VECTOR(0, 0, 120), VECTOR (0,0,0));
  //camera.setRenderParameters (3000,2048,60);
  camera.setRenderParameters (640,480,60);

  // Define some materials
  materials["orange"]  = new CSolidMaterial (COLOR (1, 0.549, 0.0), 0.05, 1, 50);
  materials["blue"]    = new CSolidMaterial (COLOR (0.0, 0.2, 0.8), 0.15, 1, 90);
  materials["r_green"] = new CSolidMaterial (COLOR (0.0, 0.8, 0.2), 0.15, 1, 90, 2.419);
  materials["yellow"]  = new CSolidMaterial (COLOR (1,1,0),0.2,1,50);
  materials["mateWhite"]=new CSolidMaterial (COLOR (1,1,1),0,0,0);
  
 
  // Add a sphere
  CSphere *sph = new CSphere(50);
  sph->setLocation (VECTOR(0,50,0));
  sph->setMaterial (materials["blue"]);

  LOAD_OCTREE ? Octree::getInstance()->push(sph) : objects.push_back (sph);
  
  sph = new CSphere(20);
  sph->setLocation(VECTOR(90,50,75));
  sph->setMaterial (materials["blue"]);
  
  LOAD_OCTREE ? Octree::getInstance()->push(sph) : objects.push_back (sph);
  
  sph = new CSphere(20);
  sph->setLocation(VECTOR(200,50,-10));
  sph->setMaterial (materials["mateWhite"]);
  
  LOAD_OCTREE ? Octree::getInstance()->push(sph) : objects.push_back (sph);

  sph = new CSphere(13);
  sph->setLocation(VECTOR(120,30,0));
  sph->setMaterial (materials["yellow"]);
  
  LOAD_OCTREE ? Octree::getInstance()->push(sph) : objects.push_back (sph);
  
  CCylinder* ccy = new CCylinder(13,30);
  ccy->setLocation(VECTOR(150,5,50));
  ccy->setMaterial (materials["mateWhite"]);
  
  //LOAD_OCTREE ? Octree::getInstance()->push(ccy) : objects.push_back (ccy);
  
  // Add the ground
  CPlane *plane = new CPlane (VECTOR(0,1,0), 0);
  plane->setMaterial (materials["orange"]);
  
  LOAD_OCTREE ? Octree::getInstance()->push(plane) : objects.push_back(plane);

  // Add a single white light
  CLight *light = new CLight(VECTOR (400,400,400), COLOR (5,1,1));
  lights.push_back (light);
  //light = new CLight(VECTOR (200,500,200), COLOR (5,1,1));
  //lights.push_back (light);

  if(LOAD_OCTREE){
    Octree::getInstance()->load();
  }
}

/*-<==>-----------------------------------------------------------------
/ MAIN
/----------------------------------------------------------------------*/
int main(int argc, char **argv) {
  /**
   * Init static classes
   */
  Stats::getInstance();
  Octree::getInstance();
  Octree::setMaxDepth(11);
  
  CRayTracer rt;
  
  Stats::clear();
  rt.loadSnowflake("/users/danibarca/balls_1.txt");
  rt.render("/users/danibarca/test1.tga");
  
  Stats::print();
  
  Stats::clear();
  rt.loadSnowflake("/users/danibarca/balls_2.txt");
  rt.render("/users/danibarca/test2.tga");
  
  Stats::print();
  
  Stats::clear();
  rt.loadSnowflake("/users/danibarca/balls_3.txt");
  rt.render("/users/danibarca/test3.tga");
  
  Stats::print();
  
  Stats::clear();
  rt.loadSnowflake("/users/danibarca/balls_4.txt");
  rt.render("/users/danibarca/test4.tga");
  
  Stats::print();
  
  /*rt.load();
  rt.render("/users/danibarca/test.tga");
  
  Stats::print();*/
  
  return 0;
}

