//
//  Utils.cpp
//  RayTracing
//
//  Created by Dani Barca on 13/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#include "Utils.h"

int round_num(double x){ return (int)((x < 0.0) ? x - 0.5 : x + 0.5); }

unsigned int fast_sqrt(unsigned int x, unsigned int ix){
  unsigned int b = x;
  unsigned int a = x = 0x3f;
  ix = ix == 0 ? 1/x : ix;
  x = b*ix;
  a = x = (x+a)>>1;
  x = b*ix;
  a = x = (x+a)>>1;
  x = b*ix;
  x = (x+a)>>1;
  return(x);
}
