//
//  Octree.cpp
//  RayTracing
//
//  Created by Dani Barca on 11/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#include "Octree.h"

Octree* Octree::instance;
O_Node* Octree::rootNode;
int Octree::max_depth;

Octree::Octree(){
  assert(instance == NULL);
  instance = this;
  
  //rootNode = new O_Node(min_x,min_y,min_z,max_x,max_y,max_z,NULL,true);
  no_octree_objs = std::vector<CRTObject*>();
  
  object_list = std::vector<CRTObject*>();
  
  min_x = -1;
  min_y = -1;
  min_z = -1;
  max_x = -1;
  max_y = -1;
  max_z = -1;
  
  max_depth = -1;
  
  loaded = false;
  first = true;
}

Octree* Octree::getInstance(){
  if(instance == NULL){
    instance = new Octree();
  }
  return instance;
}

void Octree::push(CRTObject* o){
  this->object_list.push_back(o);
  
  if(!o->is_finite) return;
  
  if(o->min_x < min_x || first){
    this->min_x = o->min_x;
  }
  if(o->min_y < min_y || first){
    this->min_y = o->min_y;
  }
  if(o->min_z < min_z || first){
    this->min_z = o->min_z;
  }
  if(o->max_x > max_x || first){
    this->max_x = o->max_x;
  }
  if(o->max_y > max_y || first){
    this->max_y = o->max_y;
  }
  if(o->max_z > max_z || first){
    this->max_z = o->max_z;
  }
  
  first = false;
}

void Octree::setMaxDepth(int max){
  max_depth = max;
}

void Octree::load(){
  assert(!first);
  rootNode = new O_Node(min_x,min_y,min_z,max_x,max_y,max_z,NULL,true);
  
  std::vector<CRTObject*>::iterator it = object_list.begin();
  
  CRTObject* obj;
  while(it != object_list.end()){
    obj = *it++;
    if(!rootNode->checkAndPush(obj)){
      no_octree_objs.push_back(obj);
    }
  }
  
  loaded = true;
}

std::vector<CRTObject*> Octree::getObjects(CLine* line){
  assert(loaded);
  
  SCALAR tmax;
  std::vector<CRTObject*> res = rootNode->getObjects(line,&tmax);
  res.insert(res.end(),no_octree_objs.begin(),no_octree_objs.end());
  return res;
}
