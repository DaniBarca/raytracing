//
//  Utils.h
//  RayTracing
//
//  Created by Dani Barca on 13/5/15.
//  Copyright (c) 2015 DBC. All rights reserved.
//

#ifndef __RayTracing__Utils__
#define __RayTracing__Utils__

#include <stdio.h>

#ifndef MULT_DEGTORAD
#define MULT_DEGTORAD 0.01745329251994329576923f
#endif

#ifndef MUTL_RADTODEG
#define MUTL_RADTODEG 57.2957795130823208767981f
#endif

#ifndef DEGTORAD
#define DEGTORAD(d) d*MULT_DEGTORAD
#endif

#ifndef RADTODEG
#define RADTODEG(r) r*MUTL_RADTODEG
#endif

int round_num(double x);

unsigned int fast_sqrt(unsigned int x, unsigned int ix = 0);

#endif /* defined(__RayTracing__Utils__) */
