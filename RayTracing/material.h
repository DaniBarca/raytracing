#ifndef INC_MATERIAL_H_
#define INC_MATERIAL_H_

#pragma warning (disable : 4786 )

/*-<==>-----------------------------------------------------------------
/ Generic Material definition
/ Some materials will not use the position to define the diffuse or 
/ reflectance values
/----------------------------------------------------------------------*/
class CMaterial {
public:
  CMaterial() {}
  virtual ~CMaterial() {}
  virtual COLOR  getDiffuseColor(const VECTOR &loc) const = 0;
  virtual SCALAR getReflectance(const VECTOR &loc) const = 0;
  virtual SCALAR getGloss() const = 0;
  virtual SCALAR getSpecular() const = 0;
  virtual SCALAR getRefractiveIndex() const = 0;
  
  // Add more methods if required
};

/*-<==>-----------------------------------------------------------------
/ A map, key is a std::string and the value is a material pointer
/----------------------------------------------------------------------*/
#include <map>
#include <string>
typedef std::map<std::string, CMaterial *> MMaterials;

/*-<==>-----------------------------------------------------------------
/ Solid color material
/----------------------------------------------------------------------*/
class CSolidMaterial : public CMaterial {
  COLOR  diffuse_color;
  SCALAR reflectance_factor;
  SCALAR specular_constant;
  SCALAR gloss;
  
  SCALAR refractive_index;
public:
  CSolidMaterial(const COLOR &diffuse, SCALAR reflectance, SCALAR specular_constant, SCALAR gloss, SCALAR refractive_index = 0);
  COLOR  getDiffuseColor(const VECTOR &loc) const;
  SCALAR getReflectance(const VECTOR &loc)  const;
  SCALAR getSpecular() const;
  SCALAR getGloss() const;
  SCALAR getRefractiveIndex() const;
};

#endif
